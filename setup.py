from setuptools import setup 
import os,glob

setup(name='virtualsat',
      description='given a cycle coverage as a geopandas, provide api to get frames at any dates or colocation',
      url='https://git.cersat.fr/oarcher/virtualsat.git',
      version = "0.0.1",
      author = "Olivier Archer",
      author_email = "Olivier.Archer@ifremer.fr",
      license='GPL',
      packages=['virtualsat'],
      zip_safe=False,
      install_requires=[ 'shapely', 'geo_shapely' ],
      include_package_data=True,
      #data_files=[ (os.path.dirname(p) , [p] ) for p in  glob.glob('var/lib/virtualsat/cycles/**',recursive=True) if not os.path.isdir(p) ]
)
