#!/usr/bin/env python
import pandas as pd
import geopandas as gpd
import numpy as np
import shapely.geometry as shp
import datetime as dt
import pickle as pkl
import glob
import sys
import os
import logging
import geopandas as gpd
import geo_shapely as geoshp

logging.basicConfig()
logger = logging.getLogger("virtualsat")
if sys.gettrace():
    logger.setLevel(logging.DEBUG)
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)
    pd.options.mode.chained_assignment = 'raise'
else:
    logger.setLevel(logging.INFO)
    


# modulo support for datetime 
class datetime(dt.datetime):
    epoch = dt.datetime.min
    def __divmod__(self, delta):
        seconds = int((self - datetime.epoch).total_seconds())
        remainder = dt.timedelta(
            seconds=seconds % delta.total_seconds(),
            microseconds=self.microsecond,
        )
        quotient = self - remainder
        return quotient, remainder

    def __floordiv__(self, delta):
        return divmod(self, delta)[0]

    def __mod__(self, delta):
        return divmod(self, delta)[1]

def read_savoir(btfid):
    """
    read a csv file in  savoir format (ie from cls)
    
    return:
        gdf
    """
    
    def str2date(s):
        d=None
        for f in ['%d-%b-%Y %H:%M:%S.%f', '%Y-%b-%d %H:%M:%S UTC.%f','%d/%m/%Y %H:%M']:
            try:
                d=dt.datetime.strptime(s, f)
                break
            except:
                pass
        
        assert d != None
        return d
    
    
    # Savoir reader
    df=pd.read_csv(btfid,usecols=[
        'NW_Lat','NW_Lon','NE_Lat','NE_Lon','SE_Lat','SE_Lon','SW_Lat','SW_Lon',
        'Start','End','Center_Lat','Center_Lon','Orbit','Cycle','RelOrb'],
        index_col=False, sep=',', parse_dates=['Start','End'],date_parser=str2date)
    df.drop_duplicates(inplace=True)
    df.rename(columns={"Start": "startdate", "End": "stopdate"}, inplace=True)
    df['geometry'] = df.apply(lambda row: geoshp.smallest_dlon(shp.Polygon([(row.NW_Lon,row.NW_Lat),(row.NE_Lon,row.NE_Lat),(row.SE_Lon,row.SE_Lat),(row.SW_Lon,row.SW_Lat)])),axis=1)
    gdf = gpd.GeoDataFrame(df[['startdate','stopdate','geometry', 'Orbit' , 'Cycle','RelOrb' ]],geometry='geometry',crs='epsg:4326')
    return gdf

        
class VirtualSat:
    def __init__(self,vsat=None,vsat_csv=None):
        self._gdf_cycle = None
        self.period = None
        self._epoch = None
        self.orbits = None
        if vsat is not None:
            vsat_csv = os.path.join(sys.prefix,os.path.dirname(__file__),"%s_cycle.csv" % vsat) 
        if vsat_csv is not None:
            self._gdf_cycle = read_savoir(open(vsat_csv,"r"))
        self._epoch = self._gdf_cycle['startdate'].min()
        self.period = self._gdf_cycle['stopdate'].max() - self._gdf_cycle['startdate'].min()
        self.orbits = self._gdf_cycle['Orbit'].unique().size
        
    def _cycle_mod(self,date):
        """ return startdate and delta of the virtual cycle for a given date (ie datetime % timedelta , datetime // timedelta"""
        seconds = int((date - self._epoch).total_seconds())
        cycle_delta = dt.timedelta(
            seconds=seconds % self.period.total_seconds(),
            microseconds=date.microsecond,
        )
        cycle_start = date - cycle_delta
        return cycle_start,cycle_delta
    
    def get_gdf(self,startdate,stopdate):
        """return a gdf with good dates in date range"""
        gdf_list=[]
        startdate_cycle, _ = self._cycle_mod(startdate)
        stopdate_cycle , _ = self._cycle_mod(stopdate)
        delta_start_cycle = ( startdate_cycle - self._epoch).total_seconds() / self.period.total_seconds()
        delta_stop_cycle = ( stopdate_cycle - self._epoch).total_seconds() / self.period.total_seconds()
        for i_cycle in range(int(delta_start_cycle),int(delta_stop_cycle+1)):
            cycle = self._gdf_cycle.copy()
            cycle['Cycle'] = cycle['Cycle'] + i_cycle
            cycle['Orbit'] = cycle['Orbit'] + i_cycle * self.orbits
            cycle['startdate'] = cycle['startdate'] + i_cycle * self.period
            cycle['stopdate'] = cycle['stopdate'] + i_cycle * self.period
            gdf_list.append(cycle)
        gdf = pd.concat(gdf_list)
        gdf = gdf[(gdf['startdate'] >= startdate) & (gdf['stopdate'] <= stopdate)]
        gdf.reset_index(inplace=True)
        return gdf
        
